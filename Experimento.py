
def principal():

    pendiente = int(input("Cual es la pendiente de la recta: "))
    valorInicialX, valorFinalX = int(input("Valor inicial X: ")), int(input("Valor final X: "))

    valoresX = range(valorInicialX, valorFinalX + 1)
    valoresXPares = list([i for i in valoresX if i % 2 ==0])
    valoresY = list(graficador(pendiente, valoresX))

    print(f'valoresY {valoresY}')
    totalValoresY = suma(*valoresY)
    print(f'totalValoresY {totalValoresY}')
    print(f'valoresXPares {valoresXPares}')


def calculaY(X, M):
    """
    Calcula el valor en Y de una recta
    X int valor en X de una recta
    M int pendiente de la recta
    returns Valor en Y de la recta 
    """
    return M*X + 5

def graficador(pendiente, valoresX):
    for posicionX in valoresX:
        yield calculaY(posicionX, pendiente)

def suma(*args):
    total=0
    for valor in args:
        total += valor
    return total

def aplicar_operaciones(num):
    operaciones = [abs, float]

    resultado = []
    for operacion in operaciones:
        resultado.append(operacion(num))

    return resultado

principal()
print(f'funciones como variable: {aplicar_operaciones(-2)}')


import unittest

class CajaNegraTest(unittest.TestCase):
    def  testCalcula_Y(self):
        pendiente = 2
        posicionX = 5

        resultado = calculaY(posicionX, pendiente)

        self.assertEqual(resultado, 15)

if __name__ == "__main__": #le indica a python que si el archivo es el script principal que se esta corriendo, se ejecute algo
    unittest.main()