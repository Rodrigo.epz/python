def morral(tamano_morral, pesos, valores, n):
    
    if n == 0 or tamano_morral == 0:
        print("    return 0 ")
        return 0

    print(f"    Indice:{n} | Morral: {tamano_morral} --> peso: {pesos[n - 1]} -- valor: {valores[n - 1]} :: {pesos}")

    if pesos[n - 1] > tamano_morral: 
        print("El peso de uno de los elementos es mayor que la pasidad del morral")
        return morral(tamano_morral, pesos, valores, n - 1)
    print(f"Ejecuta fun1 --> morral({tamano_morral} - {pesos[n - 1]}, {pesos}, {valores}, {n - 1})")
    fun1  = morral(tamano_morral - pesos[n - 1], pesos, valores, n - 1)
    print(f"    fun = {fun1}")

    print(f"Ejecuta fun2 --> morral({tamano_morral}, {pesos}, {valores}, {n - 1})")
    fun2 = morral(tamano_morral, pesos, valores, n - 1)
    print(f"    fun2 = {fun2}")

    print(f"Ingresos: {valores[n - 1]} + {fun1},{fun2} ")

    return max(valores[n - 1] + fun1, fun2 )


if __name__ == '__main__':
    valores = [60, 100,  120]
    pesos = [10, 20, 30]
    tamano_morral = 30
    n = len(valores) 

    print(f"Ejecuta funInit --> morral({tamano_morral}, {pesos}, {valores}, {n})")
    resultado = morral(tamano_morral, pesos, valores, n)
    print(resultado)