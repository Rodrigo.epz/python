import random
def tirar_dado(numero_de_tiros):
    secuencia_de_tiros = []
    for _ in range(numero_de_tiros):
        secuencia_de_tiros.append(random.choice([1,2,3,4,5,6]))
    return secuencia_de_tiros
        
def tirar_dados(numero_de_tiros):
    secuencia_de_tiros = []
    for _ in range(numero_de_tiros):
        tiro = random.choice([1,2,3,4,5,6])
        tiro2 =  random.choice([1,2,3,4,5,6])
        tiro = tiro + tiro2
        secuencia_de_tiros.append(tiro)
    return secuencia_de_tiros

def main(numero_de_tiros, numero_de_intentos):
    tiros = []
    tirosCon2Dados= []
    for _ in range(numero_de_intentos):
        tiros.append(tirar_dado(numero_de_tiros))
        tirosCon2Dados.append(tirar_dados(numero_de_tiros))
    
    tiros_con_1 = 0
    tiros_sin_1 = 0

    for tiro in tiros:
        if 1 in tiro:
            tiros_con_1 += 1
        if 1 not in tiro:
            tiros_sin_1 += 1

    probabilidad_de_tiros_con1 = tiros_con_1/numero_de_intentos
    probabilidad_de_tiros_sin1 = tiros_sin_1/numero_de_intentos

    print(f'probabilidad de obtener por lo menos un 1 en {numero_de_tiros} tiros = {probabilidad_de_tiros_con1}')
    print(f'probabilidad de no obtener por lo menos un 1 en {numero_de_tiros} tiros = {probabilidad_de_tiros_sin1}')

    tiros_con_12 = 0

    for tiro in tirosCon2Dados:
        if 12 in tiro:
            tiros_con_12 += 1
    probabilidad_De_tiros_con12 = tiros_con_12 / numero_de_intentos
    print(f'Probabilidad de obtener porlomenos un 12 en {numero_de_tiros} tiros = {probabilidad_De_tiros_con12}')




if __name__ == "__main__":
    numero_de_tiros = int(input('Cuantas veces vamos a tirar el dado: '))
    numero_de_intentos = int(input('Cuantas veces simulamos: '))

    main(numero_de_tiros, numero_de_intentos)