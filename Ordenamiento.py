import random

def ordenamiento_burbuja(lista):
    n = len(lista)

    for i in range(n):
        for j in range(0, n - i -1):
            if lista[j] > lista[j+1]:
                lista[j], lista[j+1] = lista[j+1], lista[j]

    return lista


def ordenamiento_por_insercion(lista):

    for indice in range(1, len(lista)):
        valor_actual = lista[indice]
        posicion_actual = indice

        while posicion_actual > 0 and lista[posicion_actual - 1] > valor_actual:
            lista[posicion_actual] = lista[posicion_actual - 1]
            posicion_actual -= 1

        lista[posicion_actual] = valor_actual

def ordenamiento_por_mezcla(lista, index = 0):
    if len(lista) > 1:
        medio = len(lista) // 2
        izquierda = lista[:medio]
        derecha = lista[medio:]
        print(izquierda, '*' *5, derecha)

        # llamada recursiva en cada mitad
        print('<<-- ', index)
        ordenamiento_por_mezcla(izquierda, index +1)
        print('-->> ', index)
        ordenamiento_por_mezcla(derecha, index + 1)

        #iteradores para reocrrer las sublistas
        i = 0
        j = 0
        #Iterador para la lista principal
        k = 0

        while i < len(izquierda) and j < len(derecha):
            if izquierda[i] < derecha[j]:
                lista[k] = izquierda[i]
                i += 1
            else:
                lista[k] = derecha[j]
                j += 1
            k += 1
        while i < len(izquierda):
            lista[k] = izquierda[i]
            i += 1
            k += 1
        while j < len(derecha):
            lista[k] = derecha[j]
            j += 1
            k += 1
        
        print(f'izquierda {izquierda}, derecha {derecha}')
        print(f'Lista: {lista}')
        print('-' * 20 + ' ', index)

    return lista

if __name__ == "__main__":
    tamaño_de_lista = int(input("De que tamaño sera la lista"))
    lista = [random.randint(0,100) for i in range(tamaño_de_lista)]
    print(lista)

    lista_ordenada = ordenamiento_por_mezcla(lista)
    print(lista_ordenada)