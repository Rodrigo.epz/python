def varianza(X):
    mu = media(X)
    x_mu = list(map(lambda x: (x - mu)**2, X))
    return sum(x_mu) / len(X)

def desviacion_estandar(X):
    return varianza(X)**(1/2)

def media(x):
    return sum(x)/len(x)