objetivo = int(input("Escoge un numero: "))
epsilon = 0.01
paso = epsilon**2
respuesta = 0.0

while abs(respuesta**2 - objetivo) >= epsilon and respuesta <= objetivo :
    print(abs(respuesta**2 - objetivo), respuesta)
    respuesta += paso

print(f"La raiz cuadrada de {objetivo }es {respuesta}")
"""
En la aproximacion de soluciones nos apoyamos en la variable de epsilon, la cual representa un factor de 'exactitud' para calcular la solucion de problemas que 
no requieran una solucion exacta, en este caso se calcula la raiz cuadrada de un numero aun siendo inexact, con el mismo epsilon se calcula el paso que se dara 
en cada aproximacion

Epislon es la variable que nos indica que tan cerca queremos estar de la solucion
"""