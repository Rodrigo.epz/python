"""
En la busqueda binaria requerimos de una estructura o muestra de datos ordenada, despues de esto simplemente se parte de la mitad de la muestra y hacemos la
validacion para apartir de ahi saber hacia que la do de la muestra irnos para seguir haciendo validaciones

Epsilon nos indica que tan cerca queremos estar de la solucion
"""

objetivo = int(input("Escoge un numero:"))
epsilon = 0.001
bajo = 0.0
alto = max(1.0, objetivo)
respuesta = (alto + bajo)/2

while abs(respuesta**2 -objetivo) >= epsilon:
    print(f'bajo={bajo}, alto={alto}, respuesta={respuesta}')
    if respuesta**2 < objetivo:
        bajo = respuesta
    else:
        alto = respuesta

    respuesta = (alto + bajo)/2

print(f'la raiz cuadrada de {objetivo} es {respuesta}')